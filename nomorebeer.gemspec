Gem::Specification.new do |s|
  s.name        = 'nomorebeer'
  s.version     = File.read("version")
  s.date        = Time.now.getgm.to_s.split.first
  s.summary     = "Change the license and fix the gemspec"
  s.description = "NO MORE BEER !!! It's enough !"
  s.authors     = [
    'poulet_a'
  ]
  s.email       = 'poulet_a@epitech.eu',
  s.files       = [
    'lib/nomorebeer/nomorebeer.rb',
    'lib/nomorebeer.rb',
    'version',
    'README.md',
    'Rakefile',
    'Gemfile',
    'nomorebeer.gemspec',
    'test/test.rb',
    'certs/nephos.pem',
  ]
  s.homepage    = 'https://gitlab.com/Nephos/nomorebeer'
  s.license     = 'WTFPL'
  s.cert_chain  = ['certs/nephos.pem']
  s.signing_key = File.expand_path('~/.ssh/gem-private_key.pem') if $0 =~ /gem\z/
end
